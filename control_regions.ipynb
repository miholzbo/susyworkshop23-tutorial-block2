{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "ec37c327",
   "metadata": {},
   "source": [
    "# Part 2a: Contraining important backgrounds via control regions"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "caed28a5",
   "metadata": {},
   "source": [
    "A common way for estimating backgrounds is to normalize the dominant background processed in dedicated regions, so called Control Regions, each enriched in a certain type of background. Via a simultanous fit of all CRs normalization factors are extracted. These NFs are then extrapolated to the SR(s). To verify the validity of this extroplation, Validation regions are defined. Kinematically, these are typically located \"between\" the CRs and SRs. This approach requires that all CR/VR/SR regions are orthogonal, i.e. do not share any events.\n",
    "\n",
    "An illustration of this procedure is given in the illustration below (taken from the [HistFitter](https://arxiv.org/pdf/1410.1280.pdf) paper):"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ab382900",
   "metadata": {},
   "source": [
    "<img src=\"img/CRs-new.png\" alt=\"Image-CRs\" width=\"400\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a09fb6d0",
   "metadata": {},
   "source": [
    "This approach requires to setup a statistical model for the estimated yields in the various regions, that then can be fitted to the observed data via a maximum likelihood fit. Practically speaking, the NFs will be fitted in a way so that the SM predictions will match the observed event counts in the CRs. There is a prescription how to set up the statistical model (\"HistFactory template\") but this will be introduced tomorrow in the third part of the tutorial. Hence, we'll take a very practical appraoch here and just think about NFs as an adjustable parameter which can scale up and down the processes they are associated with. The fit then chooses the values of the NFs which suit best to the data, but are agnostic how it actually does this.\n",
    "\n",
    "There are several packages who implement the building of the statistic model and provide fitting functionalities. In the SUSY group, the [HistFitter](https://github.com/histfitter) package is pretty common, while SM (and Exotics) analyses often rely on [TRExFitter](https://trexfitter-docs.web.cern.ch/trexfitter-docs/), just to name a few.\n",
    "\n",
    "For this tutorial, we will use a fairly new package called [cabinetry](https://cabinetry.readthedocs.io/en/latest/index.html) because it works fully in python and hence it extremely well suited for an interactive usage. But keep in mind: all these packages implement the same prescreptions of how to build the statistic model to describe event counts!"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b5b75fd1",
   "metadata": {},
   "source": [
    "Equipped with all of this background information, we can finally get started! First let's load cabinetry:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "44c242a6",
   "metadata": {},
   "outputs": [],
   "source": [
    "import cabinetry\n",
    "import pyhf # this is the backend of cabinetry that contains some useful functionalities"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9374b6a4",
   "metadata": {},
   "outputs": [],
   "source": [
    "# This needs to be called to get logging messages from cabinetry\n",
    "cabinetry.set_logging()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ffc093f2",
   "metadata": {},
   "source": [
    "As you might have guessed, the expected event counts are required (and their systematic variations) to build the statistical model. Hence, we need to extract the predictions for each region (i.e. the event counts) of out the ntuples. Convienently, cabinetry provides some information exactly for that. The instructions how to read out the ntuples will be saved into a config, which is just a python dictionary. Let's create a config for cabinetery step by step!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4c4926fe",
   "metadata": {},
   "outputs": [],
   "source": [
    "config = {}\n",
    "# We need one entry to hold some general information, like the name, the path to the ntuples, etc.\n",
    "# Don't wonder too much about this for now!\n",
    "config[\"General\"] = {\"Measurement\" : \"AnalysisChallenge\", \"InputPath\" : \"/eos/atlas/atlascerngroupdisk/phys-susy/AnalysisChallenge2023/{SamplePath}\", \"POI\" : \"\", \"HistogramFolder\" : \"./histograms\"}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "271875f1",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Then we need to define the samples (i.e. the processes) which are present in our model,\n",
    "# tell cabinetry where the ntuple is and how to read it (tree name, weights to apply, etc)\n",
    "weight = \"EventWeight*140\"\n",
    "config[\"Samples\"] = [\n",
    "    {\"Name\" : \"Data\", \"Tree\" : \"ntuple_THEORY\", \"SamplePath\" : \"data.root\", \"Data\" : True},\n",
    "    {\"Name\" : \"tty\", \"Tree\" : \"ntuple_THEORY\", \"SamplePath\": \"tty.root\", \"Weight\" : weight},\n",
    "    {\"Name\" : \"Wy\", \"Tree\" : \"ntuple_THEORY\", \"SamplePath\": \"Wy.root\", \"Weight\" : weight},\n",
    "]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8ca0f713",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define which sample carries a normalization factor, i.e. a free-floating parameter which will be fitted to data\n",
    "config[\"NormFactors\"] = [\n",
    "    {\"Name\" : \"mu_Wt\", \"Samples\" : \"Wy\", \"Nominal\" : 1, \"Bounds\" : [0, 10]},\n",
    "    {\"Name\" : \"mu_tty\", \"Samples\" : \"tty\", \"Nominal\" : 1, \"Bounds\" : [0, 10]},\n",
    "]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6f37f78c",
   "metadata": {},
   "source": [
    "Alright, we obviously also need to tell cabinetry how are CRs are defined, i.e. which selection to apply for these. We need a selection enriched in tty and a selection enriched in Wy events. At the same time these regions should be as close as possible to the SR (but remain orthogonal!) so that the extracted NFs are also valid for the SR.\n",
    "\n",
    "Thinking very naively, we know that tty events should contain more than one b-tagged jet while Wy events don't have a source for b-jets at \"tree level\". So let's try to require at least 2 b-jets for the tty CR and veto b-jets for the Wy CR. If we apply the same requirments MET > 400 GeV and mgammatau > 100 GeV requirements as in the SR, we wouldn't have enough events left to reliable constrain the backgrounds. Hence we need to relax these a bit and will require 250 < MET < 400 and drop the mgammatau cut for the CRs. But but will place the VRs in the kinematic regime of the SR to validate the extrapolation over MET and mgammatau.\n",
    "\n",
    "**Note:** an extrapolation over the b-jet multiplicities isn't completely trivial. For ttbar-like events this typically works quite well (and is common practice) while for Wy it's less clear that this approach holds as the b-quarks in this topology like originate from gluon splitting $g \\to b \\bar{b}$ of in initial state (radiation) gluon so the \"characteristics\" of b-veto and b-tagged Wy events might be quite different."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5e327bd5",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Note that the cut strings need to be passed slighty different in cabinetry that for ROOT\n",
    "# CRs require 250 < MET < 400\n",
    "cuts_CRtty = \"(passTrig_MET) & (nlep == 0) & (ngamma == 1) & (ntau == 1) & (isSignaltau1 == 1) & (isSignalgamma1 == 1) & (njet >= 4) & (nbjet >= 2) & (MET>250) & (MET<400)\"\n",
    "cuts_CRWy  = \"(passTrig_MET) & (nlep == 0) & (ngamma == 1) & (ntau == 1) & (isSignaltau1 == 1) & (isSignalgamma1 == 1) & (njet >= 4) & (nbjet == 0) & (MET>250) & (MET<400)\"\n",
    "# VRs require MET > 400 and mtaugamma > 100 as the SR\n",
    "cuts_VRtty = \"(passTrig_MET) & (nlep == 0) & (ngamma == 1) & (ntau == 1) & (isSignaltau1 == 1) & (isSignalgamma1 == 1) & (njet >= 4) & (nbjet >= 2) & (mtaugamma>100) & (MET>400)\"\n",
    "cuts_VRWy  = \"(passTrig_MET) & (nlep == 0) & (ngamma == 1) & (ntau == 1) & (isSignaltau1 == 1) & (isSignalgamma1 == 1) & (njet >= 4) & (nbjet == 0) & (mtaugamma>100) & (MET>400)\"\n",
    "\n",
    "config[\"Regions\"] = [\n",
    "    # Note the little \"hack\" here - we want to be our CRs to be only 1 bin so we use a\n",
    "    # randomly high value for the upper bin to include all events as there's currently no \"include overflow\" syntax\n",
    "    {\"Name\" : \"CR-tty\", \"Variable\" : \"MET\", \"Filter\": cuts_CRtty, \"Binning\" : [0, 1e6]},\n",
    "    {\"Name\" : \"CR-Wy\", \"Variable\" : \"MET\", \"Filter\": cuts_CRWy, \"Binning\" : [0, 1e6]},\n",
    "    {\"Name\" : \"VR-tty\", \"Variable\" : \"MET\", \"Filter\": cuts_VRtty, \"Binning\" : [0, 1e6]},\n",
    "    {\"Name\" : \"VR-Wy\", \"Variable\" : \"MET\", \"Filter\": cuts_VRWy, \"Binning\" : [0, 1e6]},\n",
    "]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ca68057d",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Finally, we also need to define the systematics associated with the predictions\n",
    "# There are some systematics branches in the ntuples but for the moment we will use just a flat 20% dummy systematic\n",
    "config[\"Systematics\"] = [\n",
    "    {\"Name\" : \"FlatSys\", \"Up\" : {\"Normalization\" : 0.2}, \"Down\" : {\"Normalization\" : -0.2}, \"Type\" : \"Normalization\"}\n",
    "]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0faa2f3f",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Let's check if we've defined our config correctly\n",
    "cabinetry.configuration.validate(config)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b0db6b73",
   "metadata": {},
   "outputs": [],
   "source": [
    "# We can let cabinetry print an overview of the config we just defined\n",
    "cabinetry.configuration.print_overview(config)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0c249579",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# This will read out the ntuples, i.e. create histograms that contain the event yields\n",
    "# and save them locally\n",
    "cabinetry.templates.build(config, method=\"uproot\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "83356358",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# This will do some post-processing of the histograms, e.g. some smoothing of systematics (not done in our case)\n",
    "cabinetry.templates.postprocess(config)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4309d29d",
   "metadata": {},
   "source": [
    "The next step involes to build a so-called workspace. This object contains all the information required to build the statistical model from. In cabinetry, it's actually also just a dictionary."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9ef3434f",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# This will build a workspace based on the configuration and utilizing the previously created histograms\n",
    "# (or more precicely the post-processed versions of these histograms)\n",
    "ws = cabinetry.workspace.build(config)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ef5ac449",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# As mentioned this workspace is just a dictionary so we can inspect it very easily\n",
    "from pprint import pprint\n",
    "pprint(ws)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "148bb292",
   "metadata": {},
   "outputs": [],
   "source": [
    "# We can also save this workspace to load it again at a later stage\n",
    "workspace_path = \"workspaces/tutorial.json\"\n",
    "cabinetry.workspace.save(ws, workspace_path)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7990f418",
   "metadata": {},
   "source": [
    "The workspace above contains the CRs and the VRs. As we only want to do the fit in the CRs, we should modify the workspace so it only contains these regions. There's a nice functionality for this in `pyhf` the backend of cabinetry to prune away unwanted channels (aka regions):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b824b9b7",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Note that ws_fit won't be a plain dict anymore but a pyhf.Workspace object\n",
    "ws_fit = pyhf.Workspace(ws).prune(channels=['VR-tty', 'VR-Wy'])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a3cb4454",
   "metadata": {},
   "source": [
    "Finally, let's also build the statistical model from the workspace:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a7bf283a",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "model, data = cabinetry.model_utils.model_and_data(ws_fit)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5b4478e3",
   "metadata": {},
   "source": [
    "A particular nice feature of cabinetry is that it provides a set of useful visualization tools that nicely integrate into python. Let's for example do some pre-fit plots of our CRs."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6c031e26",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get the pre-fit predictions from our model\n",
    "model_pred = cabinetry.model_utils.prediction(model)\n",
    "# This creates a data/MC plot, per default all regions are plotted\n",
    "figures = cabinetry.visualize.data_mc(model_pred, data, config=config)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "77c51bb5",
   "metadata": {},
   "source": [
    "It's pretty clear that our Wy MC overshoots the data quite substantially. On the other hand the tty MC seems to predect the data already resonably well.\n",
    "\n",
    "To get an idea how pure the CRs are in each background, we can also make a yield table from cabinetry so we don't need to read them off the plots:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b0ec334a",
   "metadata": {},
   "outputs": [],
   "source": [
    "_ = cabinetry.tabulate.yields(model_pred, data)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "05d54869",
   "metadata": {},
   "source": [
    "Alright, now we finally also want to do a fit! Actually, that's also quite simple:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f8cbb98e",
   "metadata": {},
   "outputs": [],
   "source": [
    "fit_results = cabinetry.fit.fit(model, data)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "daf1d935",
   "metadata": {},
   "source": [
    "As you can see from the output above, the final values for each of the fit parameters are reported at the end. Besides our two NFs `mu_Wt` and `mu_tty`, you'll also see some additonal parameters corresponding to the statistical and systemetic uncertainties (more details on what they mean will be given tomorrow).\n",
    "\n",
    "We can also plot the (non-statistical) fit parameters, which is extremely handy as in a fully fledged analysis you can have dozens of them:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8fa767de",
   "metadata": {},
   "outputs": [],
   "source": [
    "cabinetry.visualize.pulls(fit_results)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9f2a71fb",
   "metadata": {},
   "source": [
    "We can now also make post-fit plots by supplying our fit results to the `model_utils.prediction()` functionality:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7f8764a9",
   "metadata": {},
   "outputs": [],
   "source": [
    "model_pred_postfit = cabinetry.model_utils.prediction(model, fit_results=fit_results)\n",
    "_ = cabinetry.visualize.data_mc(model_pred_postfit, data, config=config)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2438d162",
   "metadata": {},
   "source": [
    "As you might have expected, our SM predictions are now spot-on with the data in th CRs (in principle the fit solves a system of two equations with two unknowns here which has a unique solution). What you might have also spotted is that the uncertainty bands are now substantially smaller than pre-fit. This originates in the correlations between the fitted parameters. We can visualize these correlations via the correlation matrix:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "788a33c0",
   "metadata": {},
   "outputs": [],
   "source": [
    "cabinetry.visualize.correlation_matrix(fit_results)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "dc3baa02",
   "metadata": {},
   "source": [
    "The statistical uncertainties have little correlations with the other paramaters (that's good!) while the two normalization factors and the \"dummy uncertainty\" are correlated with each other.\n",
    "\n",
    "The next task is to extrapolate the fit results to the VRs and check if the post-fit predictions match the observed data counts in these regions. For this we need to create a model which contains all regions:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c58789fa",
   "metadata": {},
   "outputs": [],
   "source": [
    "model_full, data = cabinetry.model_utils.model_and_data(ws)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "384a8344",
   "metadata": {},
   "source": [
    "The fit results of the CR-only configuration doesn't \"match\" the full workspace which includes the VRs as for example the statistical uncertainties for the VR are not contained in that. So we need to call a handy helper function to match the exisitng fit results to the full workspace:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ac9ad3d0",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Put back the VRs to the results by filling missing parameters with defaults\n",
    "fit_results_full = cabinetry.model_utils.match_fit_results(model_full, fit_results)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "71390bdf",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get the post-fit predictions and plot the VRs\n",
    "model_pred_postfit = cabinetry.model_utils.prediction(model_full, fit_results=fit_results_full)\n",
    "_ = cabinetry.visualize.data_mc(model_pred_postfit, data, config=config, channels=[\"VR-tty\", \"VR-Wy\"])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bb73239c",
   "metadata": {},
   "source": [
    "The data/MC agreement isn't absolutely terrible, but it isn't nice either. So there's definetely room for improvement here! Some possible things to improve/adjust:\n",
    "\n",
    "* The statistics in the tty CR and VR are rather on the low side, can you adjust the selections so we have more events entering these regions?\n",
    "* The Wy VR shows some slight underestimation of the data, maybe there's a way to move the Wy a bit closer to the kinematic phase space of the VR/SR?\n",
    "* We could also use some of the phase-space of the nbjet == 1 events to construct a VR\n",
    "* The signal contamination in the CRs/VRs should be negligible (typically less than 10% contamination is considered as fine), can check if that is the case?"
   ]
  }
 ],
 "metadata": {
  "@webio": {
   "lastCommId": null,
   "lastKernelId": null
  },
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
