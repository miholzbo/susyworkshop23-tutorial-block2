# SUSY Workshop '23 Tutorials: Analysis Design & Background Estimation

Welcome to the second day of the tutorial! We'll work on developing an analysis strategy for the analysis challenge of the workshop and introduce common ways for estimating the most important backgrounds.

## Running the Tutorial

This tutorial is meant to be run on SWAN (Service for Web-based ANalysis). To clone this repository into a SWAN project for you, please click on the button below:

[![Open in SWAN][image]][hyperlink]

[image]: img/open_in_swan.svg

[hyperlink]: https://swan-k8s.cern.ch/user-redirect/download?projurl=https://gitlab.cern.ch/miholzbo/susyworkshop23-tutorial-block2.git

In case you currently don't have a running session on SWAN you should get asked what software stack you want to use. Please select the "Bleeding Edge" release as we need one package in the tutorial ('cabinetry') which isn't contained in the official LCG releases. You can leave all other settings at the default values. If you already have a SWAN session runnint, please change the configuration to use the "Bleeding Edge" release, othwise parts of the tutorial will not work.


## Tutorial Overview

The hands-on part of this tutorial block consists of three subsequent Jupyter notebooks which you can go through at your own speed. As usual, the goal is not to shift+left-click through the cells as fast as possible but to also play around, make some tweaks here and there and asking questions. We'll develop only a simple analysis strategy for the signals of the analysis challenge to introduce some core concepts, which is by now means overly optimized. Hence, if you see that your tweaks seem to perform substantially better, don't hesitate to carry them through the rest of the tutorial!

Outline:
* Part 1: [Developing a simple analysis strategy](analysis_stategy.ipynb)
* Part 2a: [Contraining important backgrounds via control regions](control_regions.ipynb)
* Part 2b: [Estimating backgrounds via the ABDC method](ABCD_method.ipynb)

Clicking on each link should open a new tab in your browser with the associated Jupyter notebook.