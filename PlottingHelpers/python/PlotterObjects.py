import os, logging
import ROOT
import multiprocessing
from array import array

from ProcessNtuple import ProcessNtuple
import logUtils


ROOT.gROOT.SetBatch(True)
logging.setLoggerClass(logUtils.ColoredLogger)
logger = logging.getLogger("djangu")


class BasePlotter:
    def __init__(self, m_outputDirectory=os.getcwd(), m_cuts = ["1"], m_weight="1."):
        self.outputDirectory = m_outputDirectory
        self.cuts = m_cuts
        self.weight = m_weight
        self.processList = []
        self.jobList = []

        logger.info("Initializing plotter with:")
        logger.info(f"output directory: {self.outputDirectory}")
        logger.info(f"cuts: {self.cuts}")
        logger.info(f"weight: {self.weight}")
        if not os.path.exists(self.outputDirectory):
            logger.info("Output folder does not exist; creating it in " + self.outputDirectory)
            os.makedirs(self.outputDirectory)
        else:
            logger.warning("Output folder exists; hope you are not using the same output names by mistake")
        logger.info(" ")

    def AddProcess(self, process):
        if type(process) == Process:
            self.processList.append(process)
        else:
            print(f"argument of HistPlotter.AddProcess() was not of type Process")

    def AddNewProcess(self, name, pathToFile, tree, isMC, cuts=["1"], style="background", color = ROOT.kWhite):
        thisProcess = Process(name, pathToFile, tree, isMC, cuts, style, color)
        self.processList.append(thisProcess)
        logger.info(f"Adding process: {thisProcess.name}")
        logger.info(f"file/tree: {thisProcess.pathToFile}/{thisProcess.tree}")
        logger.info(f"type, style, color: {'MC' if thisProcess.isMC else 'Data'}, {thisProcess.style}, {thisProcess.color}")
        logger.info(" ")

    def LaunchJobs(self, n_cores=None):
        if n_cores:
            NCORES = n_cores
        else:
            NCORES = multiprocessing.cpu_count()
        logger.info(f"Launching jobs using the {NCORES} cores of this computer.")
        pool = multiprocessing.Pool(processes=NCORES)

        results = []
        files = []
        for job in self.jobList:
            results.append(pool.apply_async(ProcessNtuple, args=(job, NCORES)))
            files.append(job.pathToFile)
        logger.info("Listing processed files:")
        while len(results) != 0:
            for i, (result, f) in enumerate(zip(results, files)):
                if result.ready():
                    logger.info(files.pop(i))
                    results.pop(i)
        pool.close()
        pool.join()
        logger.info("All jobs finished.")
        logger.info(" ")


class Process:
    def __init__(self, name, pathToFile, tree, isMC, cuts=["1"], style="background", color = ROOT.kWhite):
        self.name = name
        self.label = self.name.replace(" ","_") #is this needed? Or just in self.histosFileName?
        self.pathToFile = pathToFile
        self.tree = tree
        self.isMC = isMC
        self.cuts = cuts
        self.style = style
        self.color = color
        self.histosFileName = os.path.basename(self.pathToFile).rstrip(".root") + "." + self.label + ".histos.root"


class RawHist:
    def __init__(self, varexp, binning, label="default_label"):
        self.varexp = varexp
        self.binning = FormatBinning(binning)
        self.label = label


class Job:
    def __init__(self, process, regionDict, weight, outputDirectory, histsPerRegion):
        self.pathToFile = process.pathToFile
        self.tree = process.tree
        self.histosFileName = process.histosFileName
        self.isMC = process.isMC
        self.regionDict = regionDict
        self.weight = weight
        self.outputDirectory = outputDirectory
        self.histsPerRegion = histsPerRegion
        

def FormatBinning(binning):
    m_binning = list(binning)
    for i in range(len(m_binning)):
        if type(m_binning[i]) == list: m_binning[i] = array('f', m_binning[i])   
    return tuple(m_binning)

