import logging
#import tqdm
import sys

class ColoredFormatter(logging.Formatter):
    RESET_SEQ = "\033[0m"
    COLOR_SEQ = "\033[1;%dm"
    BOLD_SEQ = "\033[1m"
    BLACK, RED, GREEN, YELLOW, BLUE, MAGENTA, CYAN, WHITE = range(8)
    #The background is set with 40 plus the number of the color, and the foreground with 30
    #These are the sequences need to get colored ouput
    COLORS = {
        'DEBUG': BLUE,
        'INFO': GREEN,
        'WARNING': YELLOW,
        'CRITICAL': RED,
        'ERROR': RED
    }

    def __init__(self, msg=None, use_color = True):
        if msg is None:
            #msg = "[$BOLD%(asctime)s$RESET][%(levelname)-19s] ($BOLD%(filename)s$RESET:%(lineno)d) %(message)s"
            msg = "[$BOLD%(asctime)s$RESET][%(levelname)-19s] (l:%(lineno)d) %(message)s"
        msg = msg.replace("$RESET", self.RESET_SEQ).replace("$BOLD", self.BOLD_SEQ)
        logging.Formatter.__init__(self, msg, "%Y-%m-%d %H:%M:%S")
        #logging.Formatter.__init__(self, msg, "%m-%d %H:%M:%S")
        self.use_color = use_color

    def format(self, record):
        levelname = record.levelname
        if self.use_color and levelname in self.COLORS:
            levelname_color = self.COLOR_SEQ % (30 + self.COLORS[levelname]) + levelname + self.RESET_SEQ
            record.levelname = levelname_color
        return logging.Formatter.format(self, record)

########################################
######################### TQDM STREAM
########################################
"""
class TqdmStream(object):

  @classmethod
  def write(_, msg):
    tqdm.tqdm.write(msg, end='')
  # is this required?
  # @classmethod
  # def flush(_):
  #   pass
# Custom logger class

class ColoredLoggerTqdm(logging.Logger):
    def __init__(self, name):
        logging.Logger.__init__(self, name, logging.INFO)
        color_formatter = ColoredFormatter()
        if '.' in name: return  #for submodules?
        console = logging.StreamHandler(stream=TqdmStream)
        console.setFormatter(color_formatter)
        self.addHandler(console)
        return
"""

class ColoredLogger(logging.Logger):
    def __init__(self, name):
        logging.Logger.__init__(self, name, logging.INFO)
        color_formatter = ColoredFormatter()
        if '.' in name: return
        console = logging.StreamHandler()
        console.setFormatter(color_formatter)
        self.addHandler(console)
        return

class ColoredLogger2Streams(logging.Logger):

    fh = logging.FileHandler('report.log', mode='w', delay=True)
    ch = logging.StreamHandler()

    def __init__(self, name):
        logging.Logger.__init__(self, name, logging.DEBUG)
        color_formatter = ColoredFormatter()
        if '.' in name: return

        self.fh.setLevel(logging.DEBUG)
        self.ch.setLevel(logging.INFO)
        self.fh.setFormatter(ColoredFormatter())
        self.ch.setFormatter(ColoredFormatter())
        self.addHandler(self.ch)
        self.addHandler(self.fh)

        return

    def setStreamLevel(self, level):
        for handler in self.handlers:
            if isinstance(handler, logging.StreamHandler):
                handler.setLevel(level)
                break
    def setFileName(self, name):
        import os
        for handler in self.handlers:
            if isinstance(handler, logging.FileHandler):
                handler.baseFilename = handler.baseFilename.replace('report.log', name)
                break


def loggerListPrint(iterable, logger, level):
    for i in iterable:
        getattr(logger, level)(i)
