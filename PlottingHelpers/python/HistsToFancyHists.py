import logging
import csv
import ROOT

import logUtils
import AtlasStyle
from PlotTypes import DrawDataMCPlot, DrawSigVsBkgPlot, DrawDefaultPlot


logging.setLoggerClass(logUtils.ColoredLogger)
logger = logging.getLogger("djangu")
ROOT.gROOT.SetBatch(True)
#ROOT.gErrorIgnoreLevel = ROOT.kWarning


def HistsToFancyHists(plotter):
    logger.info("Making final plots")
    AtlasStyle.SetAtlasStyle()
    for plot in plotter.plotList:

        if len(plot.varexp) > 1:
            logger.warning("Multi-dimensional fancy histograms are not yet implemented. Raw histograms should have been produced.")
            continue

        # Get histos, decorate, and group by type
        bList, sList, data = GetHistograms(plot, plotter)

        # Construct bStack, bSum, dataToMCRatio
        bStack = False
        if len(bList) != 0:
            bStack = ROOT.THStack()
            bSum = False
            for b in bList:
                bStack.Add(b)
                if not bSum: bSum = b.Clone()
                else: bSum.Add(b)            
        if data and len(bList) != 0:
            dataToMCRatio = data.Clone()
            dataToMCRatio.Divide(bSum)

        # Make the specified plot
        if plot.style == "DataMC":
            DrawDataMCPlot(plot, bStack, sList, data, dataToMCRatio, plotter.outputDirectory)
        elif plot.style == "SigVsBkg":
            sigList = DrawSigVsBkgPlot(plot, bStack, bSum, sList, plotter.outputDirectory)
            sigTable = list(zip([process.name for process in plotter.processList if process.style == "signal"], sigList))
            with open(plotter.outputDirectory + "/" + plot.outputName.split(".")[0] + ".csv", 'w', newline='') as csv_file:
                csv_writer = csv.writer(csv_file)
                csv_writer.writerow(['Signal', 'Significance'])
                csv_writer.writerows(sigTable)
        else:
            DrawDefaultPlot(plot, bStack, sList, data, plotter.outputDirectory)

        logger.info(f"Saved plot: {plotter.outputDirectory + '/' + plot.outputName}, style: {plot.style}")

    logger.info("Finished drawing plots")
    logger.info(" ")


def GetHistograms(plot, plotter):
    bList = []
    sList = []
    data = False
    for process in plotter.processList:
        f = ROOT.TFile(plotter.outputDirectory + "/" + process.histosFileName)
        h = f.Get(plot.label).Clone()
        h.SetDirectory(0)
        h.SetNameTitle(process.name, process.label)
        if process.style == "background":                
            h.SetFillColor(process.color)
            h.SetMarkerSize(0)
            bList.append(h)
        elif process.style == "signal":
            h.SetMarkerSize(0)
            h.SetLineStyle(ROOT.kDashed)
            h.SetLineColor(process.color)
            h.SetLineWidth(2)
            sList.append(h)
        elif process.style == "data":
            data = h
    bList = SumHistograms(bList)
    sList = SumHistograms(sList)
    bList = sorted(bList, key = lambda x: x.Integral())
    return bList, sList, data


def SumHistograms(hList):
    hSummedDict = {}
    for h in hList:
        name = h.GetName()
        if name in hSummedDict:
            hSummedDict[name].Add(h)
        else:
            hSummedDict[name] = h.Clone()
            hSummedDict[name].SetDirectory(0)
    return list(hSummedDict.values())
