import logging
import ROOT
import multiprocessing

import logUtils


ROOT.gROOT.SetBatch(True)
logging.setLoggerClass(logUtils.ColoredLogger)
logger = logging.getLogger("djangu")


def ProcessNtuple(job, NCORES):
    ROOT.ROOT.EnableImplicitMT(NCORES) # ROOT throws warnings and caps CPU Bandwith above 32.
    #logger.info(f"thread pool size: {ROOT.ROOT.GetThreadPoolSize()}")

    treePath = "/".join(job.tree.split('/')[:-1])
    treeName = job.tree.split("/")[-1]

    inChain = ROOT.TChain(treeName)
    inChain.Add(job.pathToFile + "/" + job.tree)

    histList, cutflows_e, cutflows_w = fillHists(inChain, job)

    outFile = ROOT.TFile(job.outputDirectory + "/" + job.histosFileName, "RECREATE")
    for h in histList: outFile.WriteTObject(h)


def fillHists(inChain, job):
    df = ROOT.RDataFrame(inChain)
    df = df.Define("weight_net", job.weight if job.isMC else "1.")

    cutflows_e = {}
    cutflows_w = {}
    histList = []
    for region in job.regionDict:
        incrementalCutsList = [" && ".join(job.regionDict[region][:i+1]) for i in range(len(job.regionDict[region]))]
        df_region = df.Filter(incrementalCutsList[-1])

        cutflows_e[region] = [df.Filter(partialCut).Count() for partialCut in incrementalCutsList]
        cutflows_w[region] = [df.Filter(partialCut).Sum("weight_net") for partialCut in incrementalCutsList]

        for plot in job.histsPerRegion[region]:
            obs = plot.varexp
            tmpobs = ("TMP1", "TMP2")
            hconstr = (plot.label, plot.label) + plot.binning
            if len(obs) == 1:
                histList.append(df_region.Define(tmpobs[0], obs[0]).Histo1D(hconstr, tmpobs[0], "weight_net"))
            if len(obs) == 2:
                histList.append(df_region.Define(tmpobs[0], obs[0]).Define(tmpobs[1], obs[1]).Histo2D(hconstr, tmpobs[0], tmpobs[1], "weight_net"))

    # call everything
    for region in cutflows_e:
        for i, eventValue in enumerate(cutflows_e[region]):
            cutflows_e[region][i] = eventValue.GetValue()
        for i, weightValue in enumerate(cutflows_w[region]):
            cutflows_w[region][i] = weightValue.GetValue()
    for i, hist in enumerate(histList):
        histList[i] = hist.GetValue().Clone()
    
    return histList, cutflows_e, cutflows_w
