import os, logging
import ROOT

import logUtils
from PlotterObjects import BasePlotter, Process, RawHist, Job
from HistsToFancyHists import HistsToFancyHists


logging.setLoggerClass(logUtils.ColoredLogger)
logger = logging.getLogger("djangu")
ROOT.gROOT.SetBatch(True)


class HistPlotter(BasePlotter):
    def __init__(self, outputDirectory=os.getcwd(), cuts=["1"], weight="1."):
        super().__init__(m_outputDirectory=outputDirectory, m_cuts=cuts, m_weight=weight)
        self.plotList = []

    def AddPlot(self, plot):
        if type(plot) == Plot:
            self.plotList.append(plot)
        else:
            print(f"argument of HistPlotter.AddPlot() was not of type Plot")

    def AddNewPlot(self, outputName, varexp, binning, xTitle="", yTitle="", style="none", logX=False , logY=False, **kwargs):
        thisPlot = Plot(outputName, varexp, binning, xTitle, yTitle, style, logX, logY, **kwargs)
        self.plotList.append(thisPlot)
        logger.info(f"Adding plot: {thisPlot.outputName} with style: {thisPlot.style}")

    def MakeJobList(self):
        logger.info("Making jobs from added processes and plots")
        if len(self.processList) == 0:
            logger.error("Plotter has no processes")
            exit(1)
        elif len(self.plotList) == 0:
            logger.error("Plotter has no plots")
            exit(1)
        else:
            logger.info("Input files: ")
            for process in self.processList: 
                logger.info(process.pathToFile)
                m_cuts = list(set(self.cuts + process.cuts))
                regionDict = {"nominal" : m_cuts}
                histsPerRegion = {"nominal" : self.plotList}
                self.jobList.append(Job(process, regionDict, self.weight, self.outputDirectory, histsPerRegion))
            logger.info(" ")

    def Launch(self, n_cores=None):
        self.MakeJobList()
        self.LaunchJobs(n_cores)
        HistsToFancyHists(self)
        logger.info("Thats all!")

        
class Plot(RawHist):
    def __init__(self, outputName, varexp, binning, xTitle="", yTitle="", style="none", logX=False, logY=False, **kwargs):
        m_label =  '_'.join([outputName.split('.')[0]] + [var.replace("[","").replace("]","") for var in varexp])
        super().__init__(varexp, binning, label=m_label)
        self.outputName = outputName
        self.xTitle = xTitle
        self.yTitle = yTitle
        self.style = style
        self.logX = logX
        self.logY = logY
        self.options_dict = kwargs
