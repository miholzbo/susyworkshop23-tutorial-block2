import csv, logging
import ROOT

import logUtils
import AtlasStyle

logging.setLoggerClass(logUtils.ColoredLogger)
logger = logging.getLogger("djangu")
ROOT.gROOT.SetBatch(True)
#ROOT.gErrorIgnoreLevel = ROOT.kWarning
AtlasStyle.SetAtlasStyle()

def HistsToEfficiencies(effPlotter):
    logger.info("Making final plots")
    c = ROOT.TCanvas("c", "c", 800, 600)
    pad = ROOT.TPad("p1", "p1", 0., 0., 1., 1.)
    pad.Draw()
    pad.SetTopMargin(0.1)
    pad.SetRightMargin(0.2)
    pad.cd()
    for eff in effPlotter.efficiencyList:
        eff_dir = effPlotter.outputDirectory + "/" + eff.outputSubDir + "/"
        if eff.doFit:
            outFileName = eff_dir + "995values.csv"
            outFile = open(outFileName, "w", newline='')
            writer = csv.writer(outFile)
            writer.writerow(["sample", "threshold"])
        for process in effPlotter.processList:
            logger.info(f"Making plot for efficiency: {eff.label}, process: {process.label}.")
            f = ROOT.TFile(effPlotter.outputDirectory + "/" + process.histosFileName)
            h_total = f.Get(eff.totalHist.label).Clone()
            h_pass = f.Get(eff.passHist.label).Clone()
            h_total.SetDirectory(0)
            h_pass.SetDirectory(0)
            FixNumHist(h_pass, h_total)
            logger.info(f"Making TEfficiency with statistics option: {eff.statisticsOption}")
            TEff = ROOT.TEfficiency(h_pass, h_total)
            TEff.SetStatisticOption(eff.statisticsOption)
            if eff.doFit:
                logger.info("Fitting a sigmoid to the efficiency")
                TEffFit = FitSigmoid(TEff)
                writer.writerow([process.label, TEffFit.GetX(0.995)])
            if type(h_total) in [ROOT.TH1D, ROOT.TH1F]:
                TEff.Paint("ap")
                logger.info(type(TEff))
                TEffPlot = TEff.GetPaintedGraph().Clone()
                for point in range(TEffPlot.GetN()):
                    TEffPlot.SetPointEXlow(point, 0.)
                    TEffPlot.SetPointEXhigh(point, 0.)
                TEffPlot.GetXaxis().SetTitle(eff.xTitle)
                TEffPlot.GetYaxis().SetRangeUser(0., 1.)
                TEffPlot.GetYaxis().SetTitle("Efficiency")
                TEffPlot.Draw("ap")
            elif type(h_total) in [ROOT.TH2D, ROOT.TH2F]:
                ROOT.gStyle.SetPalette(ROOT.kBird)
                TEff.Paint("a")
                TEffPlot = TEff.GetPaintedHistogram().Clone()
                TEffPlot.GetXaxis().SetTitle(eff.xTitle)
                TEffPlot.GetYaxis().SetTitle(eff.yTitle)
                TEffPlot.GetZaxis().SetTitle("Efficiency")
                TEffPlot.Draw("colz")
            if eff.doFit: TEffFit.Draw("same l")
            AtlasStyle.AtlasLabel(0.2, 0.83)
            #AtlasStyle.AtlasLumiLabel(0.2, 0.8)
            plotName = eff_dir + process.label + "_" + eff.label + ".pdf"
            c.SaveAs(plotName)
            logger.info(f"Saved plot: {plotName}")
            logger.info(" ")
        if eff.doFit:
            outFile.close()
            logger.info(f"Saved csv file: {outFileName}")
            logger.info(" ")
    logger.info("Efficiency plotting finished.")

# In the case that Num>Den due to negative weights, set Num=Den (100% eff), raise warning 
def FixNumHist(NumHist, DenHist):
    
    if type(NumHist) == ROOT.TH1D:
        for ibin in range(0, NumHist.GetNbinsX() + 2):
            if NumHist.GetBinContent(ibin) > DenHist.GetBinContent(ibin):
                logger.warning(f"For bin {ibin}, pass hist content > total hist content. Setting pass hist entry to num hist entry.")
                logger.warning(f"pass: {NumHist.GetBinContent(ibin)}, total: {NumHist.GetBinContent(ibin)}.")
                NumHist.SetBinContent(ibin, DenHist.GetBinContent(ibin))
    elif type(NumHist) == ROOT.TH2D:
        for xbin in range(0, NumHist.GetNbinsX() + 2):
            for ybin in range(0, NumHist.GetNbinsY() + 2):
                if NumHist.GetBinContent(xbin, ybin) > DenHist.GetBinContent(xbin, ybin):
                    logger.warning(f"For bin ({xbin}, {ybin}), pass hist content > total hist content. Setting pass hist entry to num hist entry.")
                    logger.warning(f"pass: {NumHist.GetBinContent(xbin, ybin)}, total: {NumHist.GetBinContent(xbin, ybin)}.")
                    NumHist.SetBinContent(xbin, ybin, DenHist.GetBinContent(xbin, ybin))                

def FitSigmoid(eff):
    eff.Paint("ap")
    effHist = eff.GetPaintedGraph().Clone()
    a, b, c = GetSigmoidIPs(effHist)
    xf = effHist.GetXaxis().GetXmax()
    function = ROOT.TF1("eff_func", "[0]/(1+TMath::Exp(-[1]*(x-[2])))+[3]", c, xf)
    function.SetParameter(0, a)
    function.SetParameter(1, b)
    function.SetParameter(2, c)
    function.SetParameter(3, 0)
    eff.Fit(function)
    return function

def GetSigmoidIPs(effPlot):
    a = effPlot.GetPointY(effPlot.GetN() - 1)
    f1 = 0.
    for ibin in range(effPlot.GetN()):
        i = ibin
        f2 = effPlot.GetPointY(ibin)
        if f2 >= a/2.: break
        f1 = f2
    x1 = effPlot.GetPointX(i - 1)
    x2 = effPlot.GetPointX(i)
    c = (x2 + x1)/2.
    b = 4./a*(f2 - f1)/(x2 - x1)
    return a, b, c
