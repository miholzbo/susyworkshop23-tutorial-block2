import ROOT
from array import array
import AtlasStyle
import numpy as np

ROOT.gROOT.SetBatch(True)

def DrawDataMCPlot(plot, bStack, sList, data, dataToMCRatio, outputDirectory):

    # Instantiate canvas/pads                  
    c = ROOT.TCanvas("c", "c", 800, 600)
    padRatio = 0.3
    pad1 = ROOT.TPad('p1', 'p1', 0., padRatio, 1., 1.)
    pad1.SetBottomMargin(0.03)
    pad1.SetTopMargin(0.1)
    pad1.SetLeftMargin(0.15)
    pad1.Draw()
    pad2 = ROOT.TPad('p2', 'p2', 0., 0., 1., padRatio)
    pad2.SetTopMargin(0.03)
    pad2.SetBottomMargin(0.5)
    pad2.SetLeftMargin(0.15)
    pad2.Draw()

    # pad1 formatting
    pad1.cd()
    blueprint_constructor = ("blueprint_" + plot.label, "") + plot.binning
    blueprint = ROOT.TH1D(*blueprint_constructor)
    blueprint.Draw()
    blueprint.GetXaxis().SetTitle("")
    blueprint.GetXaxis().SetLabelSize(0)
    blueprint.GetYaxis().SetTitle("Events")
    blueprint.GetYaxis().SetTitleSize(1. / (1 - padRatio) * blueprint.GetYaxis().GetTitleSize())
    blueprint.GetYaxis().SetTitleOffset((1 - padRatio) * blueprint.GetYaxis().GetTitleOffset())
    blueprint.GetYaxis().SetLabelSize(1. / (1 - padRatio) * blueprint.GetYaxis().GetLabelSize())
    if plot.logX: pad1.SetLogx()
    if plot.logY:
        pad1.SetLogy()
        blueprint.SetMaximum(GetMaxHistValue(bStack, sList, data) ** (1./0.7))
    else:
        blueprint.SetMaximum(GetMaxHistValue(bStack, sList, data) / 0.7)
    legend = ROOT.TLegend(0.65, 0.65, 0.88, 0.88)

    # Draw everything available on pad1                      
    if bStack:
        bStack.Draw("hist same")
        for b in bStack.GetHists()[::-1]: legend.AddEntry(b, b.GetName(), "f")
    for s in sList:
        s.Draw("hist same")
        legend.AddEntry(s, s.GetName(), "l")
    if data:
        data.Draw("e same")
        legend.AddEntry(data, data.GetName(), "p")
    legend.Draw()
    AtlasStyle.AtlasLabel(0.24, 0.8)
    AtlasStyle.AtlasLumiLabel(0.24, 0.77)
    pad1.RedrawAxis()

    # pad 2 formatting and drawing           
    pad2.cd()
    dataToMCRatio.GetXaxis().SetTitle(plot.xTitle)
    dataToMCRatio.GetXaxis().SetTitleSize(1. / padRatio * dataToMCRatio.GetXaxis().GetTitleSize())
    dataToMCRatio.GetXaxis().SetLabelSize(1. / padRatio * dataToMCRatio.GetXaxis().GetLabelSize())
#     dataToMCRatio.GetYaxis().SetRangeUser(0.76, 1.24)
    dataToMCRatio.GetYaxis().SetRangeUser(0.5, 1.5)
    dataToMCRatio.GetYaxis().SetTitle("Data/MC")
    dataToMCRatio.GetYaxis().SetTitleSize(1. / padRatio * dataToMCRatio.GetYaxis().GetTitleSize())
    dataToMCRatio.GetYaxis().SetTitleOffset(padRatio * dataToMCRatio.GetYaxis().GetTitleOffset())
    dataToMCRatio.GetYaxis().SetLabelSize(1. / padRatio * dataToMCRatio.GetYaxis().GetLabelSize())
    dataToMCRatio.GetYaxis().SetNdivisions(3)
    dataToMCRatio.Draw("e")
    refLines = [ROOT.TLine(dataToMCRatio.GetXaxis().GetXmin(), 1., dataToMCRatio.GetXaxis().GetXmax(), 1.),
                ROOT.TLine(dataToMCRatio.GetXaxis().GetXmin(), 0.8, dataToMCRatio.GetXaxis().GetXmax(), 0.8),
                ROOT.TLine(dataToMCRatio.GetXaxis().GetXmin(), 1.2, dataToMCRatio.GetXaxis().GetXmax(), 1.2)]
    refLines[0].SetLineColor(ROOT.kRed)
    refLines[1].SetLineColor(ROOT.kBlack)
    refLines[2].SetLineColor(ROOT.kBlack)
    for line in refLines:
        line.SetLineStyle(ROOT.kDotted)
        line.Draw()

    c.Update()
    c.SaveAs(outputDirectory + "/" + plot.outputName)
    c.Close()


def DrawSigVsBkgPlot(plot, bStack, bSum, sList, outputDirectory):

    # Instantiate canvas/pads                  
    c = ROOT.TCanvas("c", "c", 800, 600)
    padRatio = 0.3
    pad1 = ROOT.TPad('p1', 'p1', 0., padRatio, 1., 1.)
    pad1.SetBottomMargin(0.03)
    pad1.SetTopMargin(0.1)
    pad1.SetLeftMargin(0.15)
    pad1.Draw()
    pad2 = ROOT.TPad('p2', 'p2', 0., 0., 1., padRatio)
    pad2.SetTopMargin(0.03)
    pad2.SetBottomMargin(0.5)
    pad2.SetLeftMargin(0.15)
    pad2.Draw()

    # pad1 formatting
    pad1.cd()
    blueprint_constructor = ("blueprint_" + plot.label, "") + plot.binning
    blueprint = ROOT.TH1D(*blueprint_constructor)
    blueprint.Draw()
    blueprint.GetXaxis().SetTitle("")
    blueprint.GetXaxis().SetLabelSize(0)
    blueprint.GetYaxis().SetTitle("Events")
    blueprint.GetYaxis().SetTitleSize(1. / (1 - padRatio) * blueprint.GetYaxis().GetTitleSize())
    blueprint.GetYaxis().SetTitleOffset((1 - padRatio) * blueprint.GetYaxis().GetTitleOffset())
    blueprint.GetYaxis().SetLabelSize(1. / (1 - padRatio) * blueprint.GetYaxis().GetLabelSize())
    if plot.logX: pad1.SetLogx()
    if plot.logY:
        pad1.SetLogy()
        blueprint.SetMinimum(0.1)
        blueprint.SetMaximum(GetMaxHistValue(bStack, sList, False) ** (1./0.7) * 10 ** (3./7.))
    else:
        blueprint.SetMaximum(GetMaxHistValue(bStack, sList, False) / 0.7)
    legend = ROOT.TLegend(0.65, 0.65, 0.88, 0.88)

    # Draw everything available on pad1                      
    if bStack:
        bStack.Draw("hist same")
        for b in bStack.GetHists()[::-1]: legend.AddEntry(b, b.GetName(), "f")
    for s in sList:
        s.Draw("hist same")
        legend.AddEntry(s, s.GetName(), "l")
    legend.Draw()
    AtlasStyle.AtlasLabel(0.24, 0.8)
    AtlasStyle.AtlasLumiLabel(0.24, 0.77)
    pad1.RedrawAxis()

    # Compute significance
    Z_function = plot.options_dict.get("significance_function", None)
    sigPlotList, sigList, ZList = ComputeSignificance(bSum, sList, Z_function)

    # pad 2 formatting and drawing           
    pad2.cd()
    sigPlotList[0].GetXaxis().SetLimits(blueprint.GetXaxis().GetXmin(), blueprint.GetXaxis().GetXmax())
    sigPlotList[0].GetXaxis().SetTitle(plot.xTitle)
    sigPlotList[0].GetXaxis().SetTitleSize(1. / padRatio * sigPlotList[0].GetXaxis().GetTitleSize())
    sigPlotList[0].GetXaxis().SetLabelSize(1. / padRatio * sigPlotList[0].GetXaxis().GetLabelSize())
    sigPlotList[0].GetYaxis().SetRangeUser(0., max(ZList)*1.1)
    sigPlotList[0].GetYaxis().SetTitle("Significance") 
    sigPlotList[0].GetYaxis().SetTitleSize(1. / padRatio * sigPlotList[0].GetYaxis().GetTitleSize())
    sigPlotList[0].GetYaxis().SetTitleOffset(padRatio * sigPlotList[0].GetYaxis().GetTitleOffset())
    sigPlotList[0].GetYaxis().SetLabelSize(1. / padRatio * sigPlotList[0].GetYaxis().GetLabelSize())
    sigPlotList[0].GetYaxis().SetNdivisions(3)
    sigPlotList[0].Draw("ap")
    for sigPlot in sigPlotList[1:]:
        sigPlot.Draw("p")

    c.Update()
    c.SaveAs(outputDirectory + "/" + plot.outputName)
    c.Close()

    return sigList


def DrawDefaultPlot(plot, bStack, sList, data, outputDirectory):

    # Instantiate canvas/pads    
    c = ROOT.TCanvas("c", "c", 800, 600)
    padRatio = 0.
    pad1 = ROOT.TPad('p1', 'p1', 0., 0., 1., 1.)
    pad1.Draw()
    pad1.SetTopMargin(0.1)

    pad1.cd()
    blueprint_constructor = ("blueprint_" + plot.label, "") + plot.binning
    blueprint = ROOT.TH1D(*blueprint_constructor)
    blueprint.Draw()
    blueprint.GetXaxis().SetTitle(plot.xTitle)
    blueprint.GetYaxis().SetTitle("Events")
    blueprint.GetYaxis().SetTitleSize(1. / (1 - padRatio) * blueprint.GetYaxis().GetTitleSize())
    blueprint.GetYaxis().SetTitleOffset((1 - padRatio) * blueprint.GetYaxis().GetTitleOffset())
    blueprint.GetYaxis().SetLabelSize(1. / (1 - padRatio) * blueprint.GetYaxis().GetLabelSize())
    if plot.logX: pad1.SetLogx()
    if plot.logY:
        pad1.SetLogy()
        blueprint.SetMinimum(0.1)
        blueprint.SetMaximum(GetMaxHistValue(bStack, sList, False) ** (1./0.7) * 10 ** (3./7.))
    else:
        blueprint.SetMaximum(GetMaxHistValue(bStack, sList, data) / 0.7)
    legend = ROOT.TLegend(0.65, 0.65, 0.88, 0.88)

    # Draw everything available          
    if bStack:
        bStack.Draw("hist same")
        for b in bStack.GetHists()[::-1]: legend.AddEntry(b, b.GetName(), "f")
    for s in sList:
        s.Draw("hist same")
        legend.AddEntry(s, s.GetName(), "l")
    if data:
        data.Draw("e same")
        legend.AddEntry(data, data.GetName(), "p")
    legend.Draw()
    AtlasStyle.AtlasLabel(0.24, 0.8)
    AtlasStyle.AtlasLumiLabel(0.24, 0.77)
    pad1.RedrawAxis()

    c.Update()
    c.SaveAs(outputDirectory + "/" + plot.outputName)
    c.Close()


def GetMaxHistValue(bStack, sList, data):
    maxValueList = []
    if data: maxValueList.append(data.GetMaximum())
    if bStack: maxValueList.append(bStack.GetMaximum())
    for s in sList: maxValueList.append(s.GetMaximum())
    return max(maxValueList)


def ComputeSignificance(bSum, sList, Z_function=None):
    sigList = []
    graphList = []
    Zlist_global = []
    for s in sList:
        Zlist = []
        graph = ROOT.TGraph()
        graph.SetMarkerColor(s.GetLineColor())
        for ibin in range(1, s.GetNbinsX()+1):
            S = s.GetBinContent(ibin)
            if S <= 0.: S = 0.
            B = bSum.GetBinContent(ibin)
            if B <= 0.: B = 0.
            if B == 0.: 
                if S >= 0.5: print(f"Skipped bin with S={S} and zero B!")
                continue
            if np.isnan(S):
                print(f"S was nan for bin {ibin}")
                S=0.
            if np.isnan(B):
                print(f"B was nan for bin {ibin}. Skipping bin")
                continue
            if Z_function:
                Z = Z_function(S, B)
            else:
                Z = np.sqrt(2*((B + S)*np.log(1. + S/B) - S))
            Zlist.append(Z)
            Zlist_global.append(Z)
            graph.AddPoint(bSum.GetBinCenter(ibin), Z)
            
        sigList.append(np.sqrt(np.sum(np.multiply(Zlist, Zlist))))
        graphList.append(graph)
        
    return graphList, sigList, Zlist_global
