import os, logging
import ROOT

import logUtils
from PlotterObjects import BasePlotter, Process, RawHist, Job
from HistsToEfficiencies import HistsToEfficiencies


logging.setLoggerClass(logUtils.ColoredLogger)
logger = logging.getLogger("djangu")
ROOT.gROOT.SetBatch(True)


class EfficiencyPlotter(BasePlotter):
    def __init__(self, outputDirectory=os.getcwd(), cuts = ["1"], weight="1."):
        super().__init__(m_outputDirectory=outputDirectory, m_cuts=cuts, m_weight=weight)
        self.efficiencyList = []

    def AddEfficiency(self, efficiency):
        if type(efficiency) == Efficiency:
            self.efficiencyList.append(efficiency)
        else:
            logger.error(f"argument of EfficiencyPlotter.AddEfficiency() was not of type Efficiency")

    def AddNewEfficiency(self, outputSubDir, varexp, binning, totalCuts, passCuts, xTitle="", yTitle="", statisticsOption=ROOT.TEfficiency.kFCP, doFit=False):
        thisEfficiency = Efficiency(outputSubDir, varexp, binning, totalCuts, passCuts, xTitle, yTitle, statisticsOption, doFit)
        self.efficiencyList.append(thisEfficiency)
        logger.info(f"Adding Efficiency: {thisEfficiency.outputSubDir}")
        if not os.path.exists(self.outputDirectory + "/" + outputSubDir):
            logger.info("Output efficiency folder does not exist; creating it in " + self.outputDirectory + "/" + outputSubDir)
            os.makedirs(self.outputDirectory + "/" + outputSubDir)
        else:
            logger.warning("Output efficiency folder exists; hope you are not using the same output names by mistake")
        logger.info(" ")

    def MakeJobList(self):
        logger.info("Making jobs from added processes and efficiencies")
        if len(self.processList) == 0:
            logger.error("Plotter has no processes")
            exit(1)
        elif len(self.efficiencyList) == 0:
            logger.error("Plotter has no efficiencies")
            exit(1)
        else:
            for process in self.processList:     
                logger.info(f"For Input file: {process.pathToFile}")
                logger.info(f"Will make efficiency histograms with (pass)/(total) cuts:")
                regions = {}
                histsPerRegion = {}
                for efficiency in self.efficiencyList:
                    # get rid of redundant cuts
                    m_totalCuts = list(set(self.cuts + process.cuts + efficiency.totalCuts))
                    m_passCuts = list(set(self.cuts + process.cuts + efficiency.passCuts))
                    # fill cut regions and hists for each region
                    regions[efficiency.label + "__total"] = m_totalCuts
                    regions[efficiency.label + "__pass"] = m_passCuts
                    histsPerRegion[efficiency.totalHist.label] = [efficiency.totalHist]
                    histsPerRegion[efficiency.passHist.label] = [efficiency.passHist]
                    # log the cuts for this efficiency
                    m_totalCuts = ' && '.join([cut for cut in m_totalCuts if cut != "1"])
                    m_passCuts = ' && '.join([cut for cut in m_passCuts if cut != "1"])
                    logger.info(f"({m_passCuts})/({m_totalCuts})")
                self.jobList.append(Job(process, regions, self.weight, self.outputDirectory, histsPerRegion))
                logger.info(" ")

    def Launch(self):
        self.MakeJobList()
        self.LaunchJobs()
        HistsToEfficiencies(self)
        logger.info("Thats all!")   


class Efficiency:
    def __init__(self, outputSubDir, varexp, binning, totalCuts, passCuts, xTitle="", yTitle="", statisticsOption=ROOT.TEfficiency.kFCP ,doFit=False):
        self.label = outputSubDir.rstrip('/')
        self.outputSubDir = outputSubDir
        self.xTitle = xTitle
        self.yTitle = yTitle
        self.totalCuts = totalCuts
        self.passCuts = passCuts
        self.totalHist = RawHist(varexp, binning, self.label + "__total")
        self.passHist = RawHist(varexp, binning, self.label + "__pass")
        self.statisticsOption = statisticsOption
        self.doFit = doFit
